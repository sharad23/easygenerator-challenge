import axios from 'axios';

const httpService = async (url, method, data) => {
  try {
    const response = await axios({
      method: method,
      url: url,
      data: data,
    });
    return response.data;
  } catch (error) {
    throw error;
  }
};

export default httpService;