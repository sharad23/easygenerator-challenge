import React, {useState, useEffect} from 'react'
import Input from '../components/input.component';
import Cookies from 'js-cookie';
import { useNavigate } from 'react-router-dom';
import httpService  from '../../httpService';
import { setCookie } from '../../util';
import _ from 'lodash';

const Login = () => {
  const [formData, setFormData] = useState({
    email: '',
    password: '',
  });
  
  const [errors, setErrors] = useState({});
  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData({ ...formData, [name]: value });
  };

  const navigate = useNavigate();
  useEffect(() => { 
    if (Cookies.get('isLoggedIn')) {
      navigate('/');
    }
    return () => {}
  }, [])

  const handleSubmit = async (e) => {
    e.preventDefault();
    const newErrors = {};
    if (!formData.email.trim().match(/^[\w-]+(\.[\w-]+)*@([\w-]+\.)+[a-zA-Z]{2,7}$/)) {
      newErrors.email = 'Please enter a valid email address';
    }
    if (!formData.password.match(/^(?=.*[a-zA-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/)) {
      newErrors.password = 'Password should contain at least one alphabet, one number, one special character, and be at least 8 characters long';
    }
    if (Object.keys(newErrors).length === 0) {
      setErrors({});
      try {
        await httpService(`${process.env.REACT_APP_API_BASE_URL}${process.env.REACT_APP_SIGNIN_ENDPOINT}`, "POST", { ...formData});
      } catch (error) {
        if (_.get(error, 'response.status', 404) !== 400) {
          newErrors.serverError = 'Internal Server Error'
        } else {
          newErrors.serverError = error.response.data.message
        }
        setErrors(newErrors);
        return
      }
      setCookie()
      navigate('/');
    } else {
      setErrors(newErrors);
    }
   
  };
  return (
    <div className='auth-wrapper'>
      <div className="auth-inner">
        <form onSubmit={handleSubmit}>
        <h3>Sign In</h3>
        {errors.serverError &&  <h5><span className="server-error-msg">{errors.serverError}</span></h5> }
        <div className="mb-3">
            <label>Email address</label>
            {errors.email && <span className="error-msg">{errors.email}</span>}
            <Input
              type="email"
              name="email"
              className="form-control"
              placeholder="Enter email"
              value={formData.email}
              onChange={handleChange}
            />
            
          </div>

        <div className="mb-3">
            <label>Password</label>
            {errors.password && <span className="error-msg">{errors.password}</span>}
            <Input
              type="password"
              name="password"
              className="form-control"
              placeholder="Enter password"
              value={formData.password}
              onChange={handleChange}
            />
            
          </div>

        <div className="d-grid">
          <button type="submit" className="btn btn-primary">
            Submit
          </button>
        </div>
        <p className="forgot-password text-right">
          New User <a href="/sign-up">sign up?</a>
        </p>
        </form>
      </div>
    </div>
  )
}

export default Login