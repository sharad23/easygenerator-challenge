import React, { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import Cookies from 'js-cookie';

const Home = () => {
  const navigate = useNavigate();
  useEffect(() => { 
    if (!Cookies.get('isLoggedIn')) {
      navigate('/sign-in');
    }
    return () => {}
  }, [])

  const logout = (e) => {
    e.preventDefault();
    Cookies.remove('isLoggedIn');
    navigate('/sign-in');
  }
  
  return (
    <main role="main" className="container">
      <div className="starter-template">
        <h1>Welcome to the App. <a href="#" onClick={logout}>LogOut</a></h1>
      </div>
    </main>
  );
};

export default Home;