import React from 'react';

const Input = ({ type, placeholder, value, onChange, className, name }) => {
  return (
    <input
      name={name}
      className={className}
      type={type}
      placeholder={placeholder}
      value={value}
      onChange={onChange}
      required 
    />
  );
};

export default Input;