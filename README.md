# Project Name

This repository contains the source code for the Backend and Frontend of the Project Name application.

## Backend

The Backend of the Project Name application serves as the API server. It provides endpoints for various functionalities.The backend is running at http://localhost:8080/

### API Documentation

The API documentation is available at [http://localhost:8080/api](http://localhost:8080/api).

## Frontend

The Frontend of the Project Name application is responsible for the user interface and interaction. The frontend is running at http://localhost:3000/](http://localhost:3000/)

## Getting Started

To start the entire application (Backend and Frontend), follow these steps:

1. Make sure you have Docker installed on your machine.

2. Clone this repository:
    ```bash
    git clone <repository-url>
    ```

3. Navigate to the project directory:
    ```bash
    cd project-directory
    ```

4. Build the Docker images:
    ```bash
    docker-compose build
    ```

5. Start the application:
    ```bash
    docker-compose up
    ```
