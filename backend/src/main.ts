import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ValidationPipe } from '@nestjs/common';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import * as cookieParser from 'cookie-parser'
import * as dotenv from 'dotenv';
import { Logger } from '@nestjs/common';
import { GlobalExceptionsFilter } from './exceptions.filters';

async function bootstrap() {
  dotenv.config(); 
  const logger = new Logger('Main');
  const app = await NestFactory.create(AppModule);
  app.useGlobalPipes(new ValidationPipe());
  app.use(cookieParser())
  app.enableCors()

  const config = new DocumentBuilder()
    .setTitle('Your API Title')
    .setDescription('API description')
    .setVersion('1.0')
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('api', app, document);

  app.useGlobalFilters(new GlobalExceptionsFilter());
  await app.listen(8080);
  logger.log('Starting application...');

  // implementing gracefull shutdown
  process.on('SIGINT', async () => {
    logger.log('Received SIGINT. Starting graceful shutdown...');
    await app.close();
    logger.log('Graceful shutdown complete.');
    process.exit(0);
  });
  
}
bootstrap();
