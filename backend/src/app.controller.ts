import { Controller, Get, UseGuards, Req, Res, HttpStatus } from '@nestjs/common';
import { AuthGuard } from './auth/auth.guard';
import { ApiTags  } from '@nestjs/swagger';

@ApiTags('Profile')
@Controller('profile')
export class AppController {
  
  @Get()
  @UseGuards(AuthGuard)
  get(@Req() req, @Res() res) {
    res.status(HttpStatus.OK).json({name: req.user.name, email: req.user.email});
  } 
}