import * as mongoose from 'mongoose';

export const UserSchema = new mongoose.Schema({
  name: { type: String, required: true },
  password: { type: String, required: true },
  email: { type: String, required: true, unique: true }
});

export interface User extends mongoose.Document {
  name: string;
  password: string;
  email: string;
}