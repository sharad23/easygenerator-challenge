import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { JwtModule } from '@nestjs/jwt';
import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';
import { UserService } from './user.service';
import { UserSchema } from './user.model';
import { AuthGuard } from './auth.guard';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'User', schema: UserSchema }]),
    JwtModule.register({
      secret: process.env.BCRYPT_SECRET || 'your-secret-key',
      signOptions: { expiresIn: process.env.TOKEN_EXPIRATION || '1h' },
    }),
  ],
  providers: [AuthService, UserService, AuthGuard],
  controllers: [AuthController],
  exports: [AuthGuard, AuthService, UserService]
})
export class AuthModule {}