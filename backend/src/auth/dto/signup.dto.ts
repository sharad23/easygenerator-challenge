import { IsString, IsEmail, MinLength, IsNotEmpty } from 'class-validator';
import { IsStrongPassword } from './password.decorator';
import { ApiProperty } from '@nestjs/swagger';

export class SignupDto {

  @ApiProperty({
    example: 'John Doe',
    required: true
  })
  @IsString()
  @IsNotEmpty()
  name: string;

  @ApiProperty({
    example: '1@y123u1Q',
    required: true
  })
  @IsString()
  @IsStrongPassword({ message: 'Password must be at least 8 characters long and contain at least 1 lowercase letter, 1 uppercase letter, 1 number, and 1 special character' }) 
  @IsNotEmpty()
  password: string;

  @ApiProperty({
    example: 'john.doe@gmail.com',
    required: true
  })
  @IsEmail()
  @IsNotEmpty()
  email: string;
}