import { ApiProperty  } from '@nestjs/swagger';
import { IsString, IsNotEmpty } from 'class-validator';

export class SigninDto {
  
  @ApiProperty({
    example: 'john.doe@gmail.com',
    required: true
  })
  @IsString()
  @IsNotEmpty()
  email: string;

  @ApiProperty({
    example: '1@y123u1Q',
    required: true
  })
  @IsString()
  @IsNotEmpty()
  password: string;
}