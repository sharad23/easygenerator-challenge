import { Controller, Post, Body, HttpException, HttpStatus, Res, Logger } from '@nestjs/common';
import { AuthService } from './auth.service';
import { SignupDto } from './dto/signup.dto';
import { SigninDto } from './dto/signin.dto';
import { ApiResponse, ApiTags, ApiBody } from '@nestjs/swagger';

@ApiTags('auth')
@Controller('auth')
export class AuthController {
  
  private readonly logger = new Logger(AuthController.name);
  
  constructor(private authService: AuthService) {}

  @Post('signup')
  @ApiResponse({ status: 201, description: 'User registered successfully.' })
  @ApiBody({ type: SignupDto })
  async signup(@Body() signupDto: SignupDto, @Res() res) {
    const token = await this.authService.signup(signupDto);
    res.cookie('token', token, {
      httpOnly: true
    })
    res.status(HttpStatus.CREATED).json({message: "success" });
  }

  @Post('signin')
  @ApiResponse({ status: 200, description: 'User logged in successfully.' })
  @ApiResponse({ status: 400, description: 'Invalid credentials.' })
  @ApiBody({ type: SigninDto })
  async signin(@Body() signinDto: SigninDto, @Res() res) {
    const token = await this.authService.signin(signinDto);
    if (token) {
      res.cookie('token', token, {
        httpOnly: true
      })
      res.status(HttpStatus.OK).json({message: "success" });
    } else {
      this.logger.error(`Invalid credentials for user ${signinDto.email}`);
      throw new HttpException('Invalid credentials', HttpStatus.BAD_REQUEST);
    }
  }
}
