import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { AuthService } from './auth.service';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private readonly authService: AuthService) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const request = context.switchToHttp().getRequest();
    if (request.cookies['token']) {
      const token = request.cookies['token']
      const user = await this.authService.verifyToken(token);
      if (user) {
        request.user = user;
        return true;
      }
    }
    return false;
  }
}
