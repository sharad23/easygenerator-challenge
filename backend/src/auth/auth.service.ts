import { Injectable, HttpException, HttpStatus, Logger } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { SigninDto } from './dto/signin.dto';
import { SignupDto } from './dto/signup.dto';
import { UserService } from './user.service';
import { User } from './user.model';
import * as bcrypt from 'bcryptjs';

@Injectable()
export class AuthService {
  private readonly logger = new Logger(AuthService.name);
  
  constructor(
    private userService: UserService,
    private jwtService: JwtService,
  ) {}

  async signup(userDto: SignupDto): Promise<string> {
    const hashedPassword = await bcrypt.hash(userDto.password, 10);
    const existingEmail = await this.userService.findUserByEmail(userDto.email);
    if (existingEmail) {
      this.logger.error(`Email Already existsr ${userDto.email}`);
      throw new HttpException('Email Already exists', HttpStatus.BAD_REQUEST);
    }
    const createdUser = await this.userService.createUser({
      ...userDto,
      password: hashedPassword,
    });
    return this.generateToken(createdUser);
  }

  async signin(signinDto: SigninDto): Promise<string | null> {
    const user = await this.userService.findUserByEmail(signinDto.email);
    if (user && (await bcrypt.compare(signinDto.password, user.password))) {
      return this.generateToken(user);
    }
    return null;
  }

  private generateToken(user: User): string {
    const payload = { email: user.email };
    return this.jwtService.sign(payload);
  }

  async verifyToken(token: string) :Promise< User | null> { 
    try {
      const payload = this.jwtService.verify(token);
      const user = await this.userService.findUserByEmail(payload.email);
      if (!user) {
        return null 
      }
      return user
    } catch (error) {

      return null;
    }
  }
}
